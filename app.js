//app.js
var QQMapWX = require('pages/common/libs/qqmap-wx-jssdk.js');
var qqmapsdk;
App({
  onLaunch: function () {
    qqmapsdk = new QQMapWX({
      key: 'XKKBZ-KJMWK-L23JJ-AIXVY-7C5S5-6JFOD'//腾讯地图服务API的key
    });
    this.getDetailLocation().then(res => {
      if (res.status === 0) {
        wx.setStorageSync('location_city', res.result.address_component.city);
      }
    });
  },
  //获取位置信息
  getDetailLocation: function () {
    return new Promise(function (resolve, reject) {
      wx.getLocation({
        type: 'gcj02',
        success: function (res) {
          var latitude = res.latitude;
          var longitude = res.longitude;
          qqmapsdk.reverseGeocoder({
            location: {latitude: latitude, longitude: longitude},
            sig: 'LmoYrZolnZ5eueQdVxTb45yirCgzuD8z',//腾讯地图服务解析坐标服务的Secret key（SK）
            success(res) {
              resolve(res)
            },
            fail: function (res) {
              wx.showToast({
                title: '获取定位失败，请检查网络',
                icon: 'none',
                duration: 1500
              })
              reject(res)
            }
          })
        }
      })
    });
  },
})
